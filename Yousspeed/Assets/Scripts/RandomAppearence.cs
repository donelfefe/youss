﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAppearence : MonoBehaviour
{
    int randomChild, count;

    [HideInInspector]
    public bool trigger = false;

    void Start()
    {
        count = transform.childCount;
    }

    void Update()
    {
        randomChild = Random.Range(0, count);
        if (trigger)
        {
            transform.GetChild(randomChild).gameObject.SetActive(true);
            trigger = false;
        }
    }
}
