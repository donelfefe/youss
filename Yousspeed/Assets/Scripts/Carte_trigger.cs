﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Carte_trigger : MonoBehaviour, IPointerClickHandler
{
    Animator anim;
    public GameObject sign;
    public God god;
    public void OnPointerClick(PointerEventData eventData)
    {
        anim.Play("trigger");

        if (sign.activeSelf)
        {
            sign.transform.GetComponentInParent<RandomAppearence>().trigger = true;
            sign.SetActive(false);
            if (god.timerStarted)
            {
                ScoreManager.SCORE += ScoreManager.scaling;
            }
        } else
        {
            if (god.timerStarted)
            {
                ScoreManager.SCORE -= ScoreManager.mistake;
            }
        }
    }

    public void showLog(string log)
    {
        Debug.Log(log);
    }

    public void Awake()
    {
        anim = GetComponent<Animator>();
    }
}
