﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OnClick : MonoBehaviour, IPointerClickHandler
{
    public God god;
    public void OnPointerClick(PointerEventData eventData)
    {
        if (!god.timerStarted)
        {
            god.timerStarted = true;
        }
    }
}
