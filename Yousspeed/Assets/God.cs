﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class God : MonoBehaviour
{
    public Text score;
    public Text timer;
    public bool timerStarted = false;

    private float timerInit = 60;

    void Update()
    {
        score.text = "Score: " + ScoreManager.SCORE;
        timerInit -= Time.deltaTime * 1;
        if (timerStarted)
        {
            if (timerInit <= 0)
            {
                timer.text = "" + 60;
                timerStarted = false;
            }
            else
            {
                timer.text = (int)Mathf.Ceil(timerInit) + "";
            }
        } else
        {
            timer.text = 60 + "";
        }
    }
}
